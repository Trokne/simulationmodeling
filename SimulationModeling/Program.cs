﻿using System;
using SimulationModeling.Core.LaboratoryWork;

namespace SimulationModeling
{
    public static class Program
    {
        private static void Main() => new WorkSelector().Switch();
    }
}