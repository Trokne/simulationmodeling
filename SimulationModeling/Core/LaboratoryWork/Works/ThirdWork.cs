using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using SimulationModeling.Core.LaboratoryWork.Data;

namespace SimulationModeling.Core.LaboratoryWork.Works
{
    public class ThirdWork : LaboratoryWork
    { static int tickSize = 5;
        static int checkTime = 5;
        static int outNumber = 0;
        static int tasksNumber = 1000;
        static int taskDiff = 10;
        static int taskLen = 20;
        static List<Queue<pair<int, int>>> msgQueue = new List<Queue<pair<int, int>>>();

        static void CenterThread()
        {
            Thread.Sleep(10 * tickSize);
            while (1 == 1)
            {
                Thread.Sleep(tickSize * checkTime);
                for (int i = 0; i < outNumber; i++) {
                    lock(msgQueue[i]) msgQueue[i].Enqueue(new pair<int, int>(0, 0));
                }
                int recivedResponses = 0;
                int[] procLoad = new int[outNumber];
                while (recivedResponses != outNumber)
                {
                    for (int i = 0; i < outNumber; i++)
                    {
                        lock(msgQueue[i])
                        {
                            if (msgQueue[i].Count != 0 && msgQueue[i].Peek().First > 0)
                            {
                                procLoad[i] = msgQueue[i].Peek().Second;
                                msgQueue[i].Dequeue();
                                recivedResponses++;
                            }
                        }
                    }
                    Thread.Sleep(tickSize);
                }
                int mx = 0;
                int mn = 0;
                for (int i = 1; i < outNumber; i++)
                {
                    if (procLoad[i] > procLoad[mx]) mx = i;
                    if (procLoad[i] < procLoad[mn]) mn = i;
                }
                if (mx != mn)
                {

                    lock(msgQueue[mx])
                    {
                        msgQueue[mx].Enqueue(new pair<int, int>(-1, (procLoad[mx] - procLoad[mn]) / 2 ));
                    }


                    while (1 == 1)
                    {
                        lock (msgQueue[mx])
                        {
                            if (msgQueue[mx].Count != 0 && msgQueue[mx].Peek().First > 0)
                            {
                                int resp = msgQueue[mx].Peek().First;
                                int res = msgQueue[mx].Peek().Second;
                                msgQueue[mx].Dequeue();
                                if (resp == 41)
                                    lock(msgQueue[mn])
                                    {
                                        msgQueue[mn].Enqueue(new pair<int, int>(-5, res));
                                        Console.WriteLine("Transition task with time {0} from worker {1} to {2}", res, mn, mx);
                                        Console.WriteLine("From {0} load to {1} load", procLoad[mx], procLoad[mn]);
                                        Console.WriteLine("");
                                    }
                                Thread.Sleep(tickSize);
                                break;
                            }
                        }
                        Thread.Sleep(tickSize);
                    }
                    
                }
                for (int i = 0; i < outNumber; i++)
                    lock (msgQueue[i]) msgQueue[i].Enqueue(new pair<int, int>(-2, -2));
            }
        }

        static void WorkerThread()
        {
            int num = 0;
            lock (msgQueue)
            {
                num = msgQueue.Count();
                msgQueue.Add(new Queue<pair<int, int>>());
            }

            outNumber++;
            int curLoad = 0;
            Random rndTime = new Random();
            Random rndLoad = new Random();
            List<int> tmp;
            List<pair<int, int>> nt = new List<pair<int, int>>();
            int cur_time = 0;

            for (int i = 0; i < tasksNumber; i++)
            {
                nt.Add(new pair<int, int>(rndTime.Next() % taskDiff + (i == 0 ? 0 : nt.Last().First),
                    rndLoad.Next() % taskLen));
            }

            int ps = 0;


            Queue<int> tasks = new Queue<int>();

            while (1 == 1)
            {
                if (ps >= tasksNumber)
                    break;
                while (ps < tasksNumber && nt[ps].First <= cur_time)
                {
                    tasks.Enqueue(nt[ps].Second);
                    curLoad += nt[ps].Second;
                    ps++;
                }

                bool balance = false;
                lock (msgQueue[num])
                {
                    if (msgQueue[num].Count() != 0 && msgQueue[num].Peek().First == 0)
                    {
                        balance = true;
                        msgQueue[num].Dequeue();
                        msgQueue[num].Enqueue(new pair<int, int>(1, curLoad));
                    }
                }

                if (balance)
                {
                    while (1 == 1)
                    {
                        lock (msgQueue[num])
                        {
                            if (msgQueue[num].Count() != 0)
                            {
                                int code, resp;
                                code = msgQueue[num].Peek().First;
                                resp = msgQueue[num].Peek().Second;
                                if (code <= 0) msgQueue[num].Dequeue();
                                if (code == -2)
                                    break;
                                if (code == -5)
                                {
                                    tasks.Enqueue(resp);
                                    curLoad += resp;
                                }

                                if (code == -1)
                                {
                                    tmp = tasks.ToList();
                                    tasks.Clear();
                                    int pt = -1;
                                    for (int i = 0; i < tmp.Count(); i++)
                                    {
                                        if (tmp[i] <= resp)
                                        {
                                            if (pt == -1 || tmp[pt] < tmp[i])
                                                pt = i;
                                        }
                                    }

                                    if (pt == -1)
                                    {
                                        lock (msgQueue[num]) msgQueue[num].Enqueue(new pair<int, int>(34, 0));
                                    }
                                    else
                                    {
                                        for (int i = 0; i < tmp.Count(); i++)
                                        {
                                            if (i == pt)
                                            {
                                                lock (msgQueue[num])
                                                    msgQueue[num].Enqueue(new pair<int, int>(41, tmp[pt]));
                                                continue;
                                            }

                                            tasks.Enqueue(tmp[i]);

                                        }
                                    }

                                    tmp.Clear();
                                }
                            }
                        }

                        Thread.Sleep(tickSize);
                    }
                }

                if (tasks.Count() == 0)
                {
                    cur_time++;
                    Thread.Sleep(tickSize);

                }
                else
                {
                    int len = tasks.Dequeue();
                    cur_time += len;
                    Thread.Sleep(cur_time * tickSize);
                }
            }
        }

        public override void Run()
        {
            var wNum = 4;
            
            var workers = new Thread[wNum];
            for (var i = 0; i < wNum; i++)
                workers[i] = new Thread(WorkerThread);
            
            var center = new Thread(CenterThread);
            for (var i = 0; i < wNum; i++)
                workers[i].Start();
            
            center.Start();

            while (center.IsAlive || workers.Any(thread => thread.IsAlive)) ;
        }
    }
}