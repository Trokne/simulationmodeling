namespace SimulationModeling.Core.LaboratoryWork.Works
{
    public abstract class LaboratoryWork
    {
        public abstract void Run();
    }
}