using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using SimulationModeling.Core.Improvement;
using SimulationModeling.Core.LaboratoryWork.Data;

namespace SimulationModeling.Core.LaboratoryWork.Works
{
    public class FirstWork : LaboratoryWork
    {
        static Queue<ModelingMessage> ShopBank = new Queue<ModelingMessage>();
        static Queue<ModelingMessage> BankShop = new Queue<ModelingMessage>();
        static Queue<ModelingMessage> BuyerShop = new Queue<ModelingMessage>();
        static Queue<ModelingMessage> ShopBuyer = new Queue<ModelingMessage>();
        static Queue<ModelingMessage> BuyerBank = new Queue<ModelingMessage>();
        static Queue<ModelingMessage> BankBuyer = new Queue<ModelingMessage>();

        const int MAX_TIME = 100;
        static int money = 300;
        const int shopTime = 1;
        const int buyerTime = 2;
        const int bankTime = 3;

        static Random rnd = new Random();

        static bool finished = false;

        static void BankThread()
        {
            int time = 0;
            while (time <= MAX_TIME)
            {
                while (BuyerBank.Count == 0 || ShopBank.Count == 0)
                {
                    if (finished)
                    {
                        time = MAX_TIME + 1;
                        break;
                    }

                    lock (BankBuyer)
                        if (BankBuyer.Count == 0)
                            BankBuyer.Enqueue(new ModelingMessage(time + bankTime, "NULL", -1));
                    lock (BankShop)
                        if (BankShop.Count == 0)
                            BankShop.Enqueue(new ModelingMessage(time + bankTime, "NULL", -1));
                    Thread.Sleep(100);
                }

                if (finished) break;
                ModelingMessage bb, sb;
                lock (BuyerBank) bb = BuyerBank.Peek();
                lock (ShopBank) sb = ShopBank.Peek();
                ModelingMessage msg;
                if (bb.time < sb.time)
                {
                    lock (BuyerBank) msg = BuyerBank.Dequeue();
                    time = msg.time;
                }
                else
                {
                    lock (ShopBank) msg = ShopBank.Dequeue();

                    if (msg.type != "NULL")
                    {
                        int res;
                        if (msg.value > money)
                            res = -1;
                        else
                            res = 1;
                        if (res == 1)
                            money -= msg.value;
                        lock (BankBuyer) BankBuyer.Enqueue(new ModelingMessage(time + bankTime, "Request", res));
                    }
                    else time = msg.time;
                }
            }

            finished = true;
        }

        static void ShopThread()
        {
            int time = 0;
            while (time <= MAX_TIME)
            {
                while (BankShop.Count == 0 || BuyerShop.Count == 0)
                {
                    if (finished)
                    {
                        time = MAX_TIME + 1;
                        break;
                    }

                    lock (ShopBank)
                        if (ShopBank.Count == 0)
                            ShopBank.Enqueue(new ModelingMessage(time + shopTime, "NULL", -1));
                    lock (ShopBuyer)
                        if (ShopBuyer.Count == 0)
                            ShopBuyer.Enqueue(new ModelingMessage(time + shopTime, "NULL", -1));
                    Thread.Sleep(100);
                }

                if (finished) break;
                ModelingMessage bas, bys;
                lock (BankShop) bas = BankShop.Peek();
                lock (BuyerShop) bys = BuyerShop.Peek();
                ModelingMessage msg;
                if (bas.time < bys.time)
                {
                    lock (BankShop) msg = BankShop.Dequeue();
                    time = msg.time;
                }
                else
                {
                    lock (BuyerShop) msg = BuyerShop.Dequeue();
                    if (msg.type != "NULL")
                        lock (ShopBank)
                            ShopBank.Enqueue(new ModelingMessage(time + shopTime, "Request", msg.value));
                    else time = msg.time;
                }
            }

            finished = true;
        }

        static void BuyerThread()
        {
            int time = 0;
            int nxt_ask = 0;
            while (time <= MAX_TIME)
            {
                while (BankBuyer.Count == 0 || ShopBuyer.Count == 0)
                {
                    if (finished)
                    {
                        time = MAX_TIME + 1;
                        break;
                    }

                    lock (BuyerBank)
                        if (BuyerBank.Count == 0)
                            BuyerBank.Enqueue(new ModelingMessage(time + buyerTime, "NULL", -1));
                    lock (BuyerShop)
                        if (BuyerShop.Count == 0)
                            BuyerShop.Enqueue(new ModelingMessage(time + buyerTime, "NULL", -1));
                    Thread.Sleep(100);
                }

                if (finished) break;

                if (time >= nxt_ask)
                {
                    int q = rnd.Next(0, 100);
                    lock (BuyerShop) BuyerShop.Enqueue(new ModelingMessage(time + buyerTime, "Request", q));
                    ConsoleImprover.WriteLine($"- Запрошено {q} у.д., текущий остаток {money}");
                    nxt_ask += 10;
                }

                ModelingMessage bb, sb;
                lock (BankBuyer) bb = BankBuyer.Peek();
                lock (ShopBuyer) sb = ShopBuyer.Peek();
                ModelingMessage msg;
                if (sb.time < bb.time)
                {
                    lock (ShopBuyer) msg = ShopBuyer.Dequeue();
                    time = msg.time;
                }
                else
                {
                    lock (BankBuyer) msg = BankBuyer.Dequeue();
                    if (msg.type != "NULL")
                    {
                        if (msg.value == 1)
                            ConsoleImprover.Write("Покупка одобрена!", ConsoleColor.DarkGreen);
                        else
                            ConsoleImprover.Write("Покупка отклонена!", ConsoleColor.DarkRed);
                        ConsoleImprover.WriteLine($". Остаток средств = {money}");
                    }
                    else time = msg.time;
                }
            }

            finished = true;
        }

        public override void Run()
        {
            var threads = new[]
            {
                new Thread(BankThread),
                new Thread(BuyerThread),
                new Thread(ShopThread)
            };

            foreach (var thread in threads)
            {
                thread.Start();
            }

            while (threads.Any(thread => thread.IsAlive)) ;
        }
    }
}