using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using SimulationModeling.Core.Improvement;
using SimulationModeling.Core.LaboratoryWork.Data;

namespace SimulationModeling.Core.LaboratoryWork.Works
{
    public class SecondWork : LaboratoryWork
    {
        static MyQueue[] que = new MyQueue[6];
        /// <summary>
        /// 0 bank user
        /// 1 user bak
        /// 2 bank shop
        /// 3 shop bank
        /// 4 user shop
        /// 5 shop user
        /// </summary>
        /// 

        const int MAX_TIME = 100;
        static int money = 1000;
        const int shopTime = 1;
        const int buyerTime = 2;
        const int bankTime = 3;
        static int gvt = 0;

        static Random rnd = new Random();

        static bool finished = false;

        static SortedDictionary<KeyValuePair<int, int>, List<int>> savedStates = new SortedDictionary<KeyValuePair<int, int>, List<int>>();
        static void SaveState(int me, int time)
        {
            KeyValuePair<int, int> k = new KeyValuePair<int, int>(time, me);
            List<int> pt = new List<int>();
            for (int i = 0; i < 6; i++)
            {
                pt.Add(que[i].p1);
                pt.Add(que[i].p2);
            }
        }

        static void CleanBeforeGVT()
        {
            SortedDictionary<KeyValuePair<int, int>, List<int>> tmp = new SortedDictionary<KeyValuePair<int, int>, List<int>>();
            foreach (   KeyValuePair<KeyValuePair<int, int>, List<int>> item in savedStates)
            {
                if (item.Key.Key >= gvt)
                    tmp.Add(item.Key, item.Value);
            }
            savedStates = tmp;
        }

        static void ReverseState(int me, int time)
        {
            KeyValuePair<int, int> k = new KeyValuePair<int, int>(time, me);
            List<int> pt = savedStates[k];
            for (int i = 0; i < 6; i++)
            {
                que[i].p1 = pt[2 * i];
                que[i].p2 = pt[2 * i + 1];
                que[i].m.RemoveRange(que[i].p2, que[i].m.Count - que[i].p2);
            }
        }

        public void CleanUntil(int me, int time)
        {
            for (int i = 0; i < 3; i++)
                for (int j = gvt + 1; j <= time; j++)
                    if (savedStates.ContainsKey(new KeyValuePair<int, int>(j, i))) savedStates.Remove(new KeyValuePair<int, int>(j, i));
        }

        static void RecalculateGVT(int nt)
        {
            int tmp = nt;
            for (int i = 0; i < 6; i++)
                for (int j = que[i].p1; j < que[i].p2; j++)
                    tmp = System.Math.Min(tmp, que[i].m[j].time);
            gvt = tmp;
        }

        static void BankThread()
        {
            int time = 0;
            while (time <= MAX_TIME)
            {
                ModelingMessage ms = new ModelingMessage(-1, "", -1);
                bool nothing = true;
                int from = -1;
                while (nothing == true)
                {
                    if (finished) { time = MAX_TIME + 1; break; }
                    lock (que[1])
                    {
                        lock (que[3])
                        {
                            if (!que[1].empty() || !que[3].empty())
                                nothing = false;
                            if (nothing == false)
                            {
                                if (!que[1].empty())
                                {
                                    if (!que[3].empty() && que[3].top().time < que[1].top().time)
                                    { ms = que[3].next(); from = 3; }
                                    else
                                    { ms = que[1].next(); from = 1; }
                                }
                                else
                                { ms = que[3].next(); from = 3; }
                            }
                        }
                    }
                    if (nothing == true) Thread.Sleep(100);
                }
                if (finished) break;
                if (ms.time < time)
                {
                    ReverseState(0, time);
                    time = gvt;
                }
                if (from == 1)
                    continue;
                int res;
                if (ms.value > money)
                    res = -1;
                else
                    res = 1;
                if (res == 1)
                    money -= ms.value;
                lock (que[0]) que[0].add(new ModelingMessage(time + bankTime, "Request", res));
                time = ms.time;
                RecalculateGVT(time + bankTime);
                CleanBeforeGVT();
                SaveState(0, time);
                
            }
            finished = true;
        }

        static void ShopThread()
        {
            int time = 0;
            while (time <= MAX_TIME)
            {
                ModelingMessage ms = new ModelingMessage(-1, "", -1);
                bool nothing = true;
                int from = -1;
                while (nothing == true)
                {
                    if (finished) { time = MAX_TIME + 1; break; }
                    lock (que[2])
                    {
                        lock (que[4])
                        {
                            if (!que[2].empty() || !que[4].empty())
                                nothing = false;
                            if (nothing == false)
                            {
                                if (!que[2].empty())
                                {
                                    if (!que[4].empty() && que[4].top().time < que[2].top().time)
                                    { ms = que[4].next(); from = 4; }
                                    else
                                    { ms = que[2].next(); from = 2; }
                                }
                                else
                                { ms = que[4].next(); from = 4; }
                            }
                        }
                    }
                    if (nothing == true) Thread.Sleep(100);
                }
                if (finished) break;
                if (ms.time < time)
                {
                    ReverseState(1, time);
                    time = gvt;
                }
                if (from == 2)
                    continue;
                lock (que[3]) que[3].add(new ModelingMessage(time + shopTime, "Request", ms.value));
                time = ms.time;
                RecalculateGVT(time + shopTime);
                CleanBeforeGVT();
                SaveState(1, time);
            }
            finished = true;
        }

        static void BuyerThread()
        {

            int time = 0;
            while (time <= MAX_TIME)
            {
                ModelingMessage ms = new ModelingMessage(-1, "", -1);
                bool nothing = true;
                int from = -1;

                while (nothing == true)
                {
                    if (finished) { time = MAX_TIME + 1; break; }
                    lock (que[0])
                    {
                        lock (que[5])
                        {
                            if (!que[0].empty() || !que[5].empty())
                                nothing = false;
                            if (nothing == false)
                            {
                                if (!que[0].empty())
                                {
                                    if (!que[5].empty() && que[5].top().time < que[0].top().time)
                                    { ms = que[5].next(); from = 5; }
                                    else
                                    { ms = que[0].next(); from = 0; }
                                }
                                else
                                { ms = que[5].next(); from = 5; }
                            }
                        }
                    }
                    if (nothing == true)
                    {
                        int q = rnd.Next(0, 100);
                        lock (que[4]) que[4].add(new ModelingMessage(time + buyerTime, "Request", q));
                        ConsoleImprover.WriteLine($"- Запрошено {q} у.д., текущий остаток {money}");
                        Thread.Sleep(200);
                    }
                }
                if (finished) break;
                if (ms.time < time)
                {
                    ReverseState(2, time);
                    time = gvt;
                }
                if (from == 5)
                    continue;

                if (ms.value == 1)
                    ConsoleImprover.Write("Покупка одобрена!", ConsoleColor.DarkGreen);
                else
                    ConsoleImprover.Write("Покупка отклонена!", ConsoleColor.DarkRed);
                ConsoleImprover.WriteLine($". Остаток средств = {money}");
                
                time = ms.time;
                RecalculateGVT(time + buyerTime);
                CleanBeforeGVT();
                SaveState(2, time);
            }
            finished = true;        
        }
        
        public override void Run()
        {
            for (var i = 0; i < 6; i++)
                que[i] = new MyQueue();
            
            var threads = new[]
            {
                new Thread(BankThread),
                new Thread(BuyerThread),
                new Thread(ShopThread)
            };

            foreach (var thread in threads)
            {
                thread.Start();
            }

            while (threads.Any(thread => thread.IsAlive)) ;
        }
    }
}