namespace SimulationModeling.Core.LaboratoryWork.Data
{
    public class ModelingMessage
    {
        public int time { get; }
        public string type { get; }
        public int value { get; }

        public ModelingMessage (int t, string tp, int v)
        {
            time = t;
            type = tp;
            value = v;
        }
    }
}