using System.Collections.Generic;

namespace SimulationModeling.Core.LaboratoryWork.Data
{
    public class MyQueue
    {
        public List<ModelingMessage> m = new List<ModelingMessage>();
        public int p1 = 0;
        public int p2 = 0;

        public MyQueue()
        {

        }
        public bool empty()
        {
            return p1 == p2;
        }
        public void add(ModelingMessage msg)
        {
            m.Add(msg);
            p2++;
        }
        public ModelingMessage next()
        {
            p1++;
            return this.m[p1 - 1];
        }

        public ModelingMessage top()
        {
            return this.m[p1];
        }

    }
}