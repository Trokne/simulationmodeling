using System;
using SimulationModeling.Core.Improvement;
using SimulationModeling.Core.LaboratoryWork.Works;

namespace SimulationModeling.Core.LaboratoryWork
{
    public class WorkSelector
    {
        private enum TaskNumber
        {
            First = 1, 
            Second, 
            Third
        }
        
        public void Switch()
        {
            PrintStartWords();

            while (true)
            {
                var textNumber = Console.ReadLine();

                if (!int.TryParse(textNumber, out var number)) 
                    continue;

                var task = (TaskNumber) number;
                SelectTask(task);
                
                return;
            }
        }

        private static void PrintStartWords()
        {
            ConsoleImprover.WriteLine("ВЫБЕРИТЕ ЛАБОРАТОРНУЮ РАБОТУ:", ConsoleColor.DarkGreen);
            ConsoleImprover.WriteLine("1. описание", ConsoleColor.DarkGreen);
            ConsoleImprover.WriteLine("2. описание", ConsoleColor.DarkGreen);
            ConsoleImprover.WriteLine("3. описание", ConsoleColor.DarkGreen);
            ConsoleImprover.WriteLine();
        }

        private void SelectTask(TaskNumber task)
        {
            Works.LaboratoryWork work;
            
            switch (task)
            {
                case TaskNumber.First:
                    work = new FirstWork();
                    break;
                case TaskNumber.Second:
                    work = new SecondWork();
                    break;
                case TaskNumber.Third:
                    work = new ThirdWork();
                    break;
                default:
                    ConsoleImprover.WriteLine("Такой лабораторной не существует :(", ConsoleColor.DarkRed);
                    return;
            }

            try
            {
                work.Run();
                ConsoleImprover.WriteLine();
                ConsoleImprover.WriteLine("- - - РАБОТА ЗАВЕРШЕНА. Спасибо! - - - \n", ConsoleColor.DarkGreen);
            }
            catch (Exception e)
            {
                ConsoleImprover.WriteLine(e.Message, ConsoleColor.DarkRed);
            }
        }
    }
}