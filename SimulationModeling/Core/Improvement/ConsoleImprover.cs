using System;

namespace SimulationModeling.Core.Improvement
{
    public static class ConsoleImprover
    {
        private static readonly ConsoleColor DefaultForegroundColor;

        static ConsoleImprover()
        {
            DefaultForegroundColor = Console.ForegroundColor;
        }
        
        public static void Write(string message, ConsoleColor foregroundColor)
        {
            Console.ForegroundColor = foregroundColor;
            Console.Write(message);
            Console.ForegroundColor = DefaultForegroundColor;

        }
        
        public static void WriteLine(string message, ConsoleColor foregroundColor)
        {
            Console.ForegroundColor = foregroundColor;
            Console.WriteLine(message);
            Console.ForegroundColor = DefaultForegroundColor;
        }
        
        public static void WriteLine() => Console.WriteLine("");

        public static void Write(string message) 
            => Write(message, DefaultForegroundColor);
        
        public static void WriteLine(string message)
            => WriteLine(message, DefaultForegroundColor);
    }
}